package Raças;
import java.util.Scanner;
import Ficha.Personagem;
import Ficha.Resistencia;
import Talentos.TalentosDeCombate;

public class Humano {

	private int Op, cont = 1;
	private boolean passFOR, passDES, passCON, passINT, passSAB, passCAR;
	
	Scanner scanner = new Scanner(System.in);

	public Humano(Ficha.Perícias pe) {
		pe.addQtdPericia(2);
	}
	
	public void EscolherHabilidade(Personagem p, Resistencia r, TalentosDeCombate t) {

		while (cont <= 2) {
			System.out.println("+2 em Duas Habilidades");
			Op = scanner.nextInt();

			switch (Op) {
			case 1:
				if (passFOR == false) {
					System.out.println("+2 FOR");
					p.addFOR(2);
					passFOR = true;
					cont++;
				}
				break;
			case 2:
				if (passDES == false) {
					System.out.println("+2 DES");
					p.addDES(2, r, p);
					passDES = true;
					cont++;
				}
				break;
			case 3:
				if (passCON == false) {
					System.out.println("+2 CON");
					p.addCON(2, r, p);
					passCON = true;
					cont++;
				}
				break;
			case 4:
				if (passINT == false) {
					System.out.println("+2 INT");
					p.addINT(2);
					passINT = true;
					cont++;
				}
				break;
			case 5:
				if (passSAB == false) {
					System.out.println("+2 SAB");
					p.addSAB(2, r, p);
					passSAB = true;
					cont++;
				}
				break;
			case 6:
				if (passCAR == false) {
					System.out.println("+2 CAR");
					p.addCAR(2);
					passCAR = true;
					cont++;
				}
				break;
			}
		}
		t.FortitudeMaior(r, p); //Tirar Isso Depois, Foi pra Teste
	}
}
