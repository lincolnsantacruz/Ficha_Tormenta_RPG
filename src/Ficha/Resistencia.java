package Ficha;

public class Resistencia {

	private int FORT;
	private int REF;
	private int VON;

	public Resistencia(Personagem p) {
		setFORT(p);
		setREF(p);
		setVON(p);
	}

	public int getFORT() {
		return FORT;
	}

	public int getREF() {
		return REF;
	}

	public int getVON() {
		return VON;
	}

	public void setFORT(Personagem p) {
		FORT = (p.getNivel() / 2) + p.getModCON();
	}
	
	public void setREF(Personagem p) {
		REF = (p.getNivel() / 2) + p.getModDES();
	}

	public void setVON(Personagem p) {
		VON = (p.getNivel() / 2) + p.getModSAB();
	}

	public void addFORT(int outros) {
		FORT = getFORT() + outros;
	}

	public void addREF(int outros) {
		REF = getREF() + outros;
	}

	public void addVON(int outros) {
		VON = getVON() + outros;
	}

}
