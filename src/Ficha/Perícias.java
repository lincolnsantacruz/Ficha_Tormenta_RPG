package Ficha;
import java.util.ArrayList;

public class Perícias {

	private int Grad, NGrad;
	private int Total, qtdPericia;
	private ArrayList<String> PericiasDeClasse = new ArrayList<String>();
	
	
	public Perícias(Personagem p) {
		setGrad(p);
		setNGrad(p);
		setQtdPerriaa(p);
	}

	public void criarPericia(String paramNomePericia, int pMod, boolean pTreinadaNTreinada) {
		PericiasDeClasse.add(paramNomePericia);
		if (pTreinadaNTreinada == true) { // � Treinada
			setTotal(getGrad() + 3 + pMod);
		} else //Ver com Marcos Depois
			setTotal(getNGrad() + pMod); // � Treinada
	}
	
	public void setGrad(Personagem p){
		Grad = p.getNivel() + 3;
	}
	
	public int getGrad(){
		return Grad;
	}
	
	public void setNGrad(Personagem p){
		Grad = p.getNivel() + 3;
	}
	
	public int getNGrad(){
		return NGrad;
	}
	
	public void setTotal(int paramTotal) {
		Total = paramTotal;
	}
	
	public int getTotal(){	
		return Total;
	}
	
	public void addPerícias(String paramNomePerícias){
		PericiasDeClasse.add(paramNomePerícias);
	}

	public void addOutros(int pOutros) {
		Total = getTotal() + pOutros;
	}
	
	public int getQtdPericiaa(){
		return qtdPericia;
	}
	
	public void setQtdPerriaa(Personagem p){
		qtdPericia = p.getModINT();
	}
	
	public void addQtdPericia(int pOutro){
		qtdPericia = getQtdPericiaa() + pOutro;
	}
}
