package Ficha;

public class Personagem extends Habilidades {
	
 public String nomeDoPersonagem;
 public String nomeJogador;
 public String Raca;//Nao eh string usar repositorio
 public String Classe; //Repositorio Agregacao SET
 public int Nivel; // ENUM 1 - 20
 public String Tendencia; // Depende da Classe ENUM
 public String Sexo; //ENUM
 public int idade; // Calculado por raça
 public String Divindade; // Só será usado caso seja Devoto
 public String Tamanho; // Depende da Raça
 public int Deslocamento; // Depende da Raça e Armadura
 private int ClasseArmadura;	
	
	public Personagem() {
	}

	/*public Personagem(int pFOR, int pDES, int pCON, int pINT, int pSAB, int pCAR) {
		super(pFOR, pDES, pCON, pINT, pSAB, pCAR);
		// TODO Auto-generated constructor stub
	}*/

	public int totalClasseArmadura(){
		ClasseArmadura = 10 + (Nivel/2) + getModDES(); // Falta implementar Armadura, escudo e Outros, al�m de penalidade
		return ClasseArmadura;
	}
	
	public int pontosDeHabilidadePorNivel(){
		int PontosExtras = Nivel/2;
		return PontosExtras;
	}

	public String getNomeDoPersonagem() {
		return nomeDoPersonagem;
	}

	public void setNomeDoPersonagem(String nomeDoPersonagem) {
		this.nomeDoPersonagem = nomeDoPersonagem;
	}

	public String getNomeJogador() {
		return nomeJogador;
	}

	public void setNomeJogador(String nomeJogador) {
		this.nomeJogador = nomeJogador;
	}

	public String getRaca() {
		return Raca;
	}

	public void setRaca(String raca) {
		Raca = raca;
	}

	public String getClasse() {
		return Classe;
	}

	public void setClasse(String classe) {
		Classe = classe;
	}

	public int getNivel() {
		return Nivel;
	}

	public void setNivel(int nivel) {
		Nivel = nivel;
	}

	public String getTendencia() {
		return Tendencia;
	}

	public void setTendencia(String tendencia) {
		Tendencia = tendencia;
	}

	public String getSexo() {
		return Sexo;
	}

	public void setSexo(String sexo) {
		Sexo = sexo;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getDivindade() {
		return Divindade;
	}

	public void setDivindade(String divindade) {
		Divindade = divindade;
	}

	public String getTamanho() {
		return Tamanho;
	}

	public void setTamanho(String tamanho) {
		Tamanho = tamanho;
	}

	public int getDeslocamento() {
		return Deslocamento;
	}

	public void setDeslocamento(int deslocamento) {
		Deslocamento = deslocamento;
	}

	public int getClasseArmadura() {
		return ClasseArmadura;
	}

	public void setClasseArmadura(int classeArmadura) {
		ClasseArmadura = classeArmadura;
	}
	
	//TODO Pedir a explica��o de INTERFACE
	//TODO Metodo Upar
	//TODO Colocar HP(vida) aqui e passar por referencia para classe guerreiro
	//TODO Fazer um decrementador de HP
	//TODO Fazer um decrementador de MP para classe conjuradoras
}
