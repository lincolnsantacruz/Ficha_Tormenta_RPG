package Talentos;

public class TalentosGerais {
	
	private String nomeTalento, Descrição;

	public String getDescrição() {
		return Descrição;
	}

	public void setDescrição(String descrição) {
		Descrição = descrição;
	}

	public String getNomeTalento() {
		return nomeTalento;
	}

	public void setNomeTalento(String nomeTalento) {
		this.nomeTalento = nomeTalento;
	}
	
}
