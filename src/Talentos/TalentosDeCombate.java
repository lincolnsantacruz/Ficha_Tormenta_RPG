package Talentos;

import Ficha.Personagem;
import Ficha.Resistencia;

public class TalentosDeCombate extends TalentosGerais {
	
	public TalentosDeCombate(){	
	}
	
	public void FortitudeMaior(Resistencia r, Personagem p){
		setNomeTalento("Fortitude Maior");
		r.addFORT(2);
	}
	
	public void ReflexosRápidos(Resistencia r, Personagem p){
		setNomeTalento("Reflexos R�pidos");
		r.addREF(2);
	}
	
	public void VontadeDeFerro(Resistencia r, Personagem p){
		setNomeTalento("Vontade de Ferro");
		r.addVON(2);
	}
}
