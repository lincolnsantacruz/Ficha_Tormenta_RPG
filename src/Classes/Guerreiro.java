package Classes;
import java.util.Scanner;

import Ficha.Personagem;
import Ficha.Perícias;
import Ficha.Resistencia;
import Talentos.TalentosDeCombate;

public class Guerreiro {

	private int BBA, op;
	private int vida;
	Scanner scanner = new Scanner(System.in);
	// private int qtdPericia;
	// private int qtdNRepeatPericias[];

	public Guerreiro(Personagem p, Perícias pe) {
		setBBA(p);
		pe.addQtdPericia(2);
	}
	
	public void CriarNovoGuerreiro(Personagem p, Perícias pe, Resistencia r, TalentosDeCombate t) {

		setVida(p);
		t.FortitudeMaior(r, p);

		while (pe.getQtdPericiaa() != 0) { // TODO Melhorar isso aqui, N�o achei
											// Legal
			System.out.println("Per�cias de Classe:");
			System.out.println("1-Adestrar Animais (Car)");
			System.out.println("2-Atletismo (For)");
			System.out.println("3-Cavalgar (Des)");
			System.out.println("4-Iniciativa (Des)");
			System.out.println("5-Intimida��o (Car)");
			System.out.println("6-Of�cio (Int)");
			System.out.println("7-Percep��o (Sab)");

			switch (op) {
			case 1:
				pe.addPerícias("Adestrar Animais");
				pe.criarPericia("Adestrar Animais", p.getModCAR(), true);
				break;
			case 2:
				pe.addPerícias("Atlerismo");
				pe.criarPericia("Atletismo", p.getModFOR(), true);
				break;
			case 3:
				pe.addPerícias("Cavalgar");
				pe.criarPericia("Cavalgar", p.getModDES(), true);
				break;
			case 4:
				pe.addPerícias("Iniciativa");
				pe.criarPericia("Iniciativa", p.getModDES(), true);
				break;
			case 5:
				pe.addPerícias("Intimida��o");
				pe.criarPericia("Intimida��o", p.getModCAR(), true);
				break;
			case 6:
				pe.addPerícias("Of�cio");
				pe.criarPericia("Of�cio", p.getModINT(), true);
				break;
			case 7:
				pe.addPerícias("Persep��o");
				pe.criarPericia("Persep��o", p.getModSAB(), true);
				break;
			}
		}
		System.out.println("Ganha Talento");
	}

	public void setBBA(Personagem p) {
		BBA = p.getNivel();
	}

	public int getBBA() {
		return BBA;
	}

	public int getVida(Personagem p) {
		setVida(p);
		return vida;
	}

	public void setVida(Personagem p) {
		if (p.getNivel() == 1)
			vida = (p.getModCON() + 20);
		else
			vida = (p.getModCON() + 20) + ((p.getNivel() - 1) * (p.getModCON() + 5));
	}

	/*
	 * if (getNivel() % 2 == 0 || getNivel() == 1) {
	 * System.out.println("Tecnica de Luta"); // Implementar Metodo para //
	 * chamar Talentos de // Combate e colocar quando Upar
	 */

}
